<?php
session_start(); 
if(isset($_SESSION['status']))
{
	$mobile=$_SESSION['status'];
	$folders=array(
		"9885776740"=>"Click&Pledge",
		"9000237738"=>"Accenture11",
		"8885444401"=>"Swayam",
		"9032760776"=>"SportsIt",
		"9581112311"=>"Ferventz",
		"9908244228"=>"PTG11"
	);
	$fpath = $folders[$mobile];
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Team Upload | Cricket Tournament</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.html">Cricket Tournament</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.html">Home</a></li>
      <li><a href="schedule.html">Schedule</a></li>
      <li><a href="teams.php">Teams</a></li>
	  <?php 
		if(isset($_SESSION['status']))
		{
			?>
				<li><a href="">Welcome Captain!</a></li>
				<li><a href="logout.php">Logout</a></li>
			<?php
		}
		else
		{
			?>
				<li><a href="login.php">Login</a></li>
			<?php
		}
	  ?>
      
    </ul>
  </div>
</nav>
  
<div class="container">
	<div class="row">
		<div class="col">
		<h3><?php echo $fpath;?> Edit Team</h3>
<?php 
$status=0;
if(isset($_POST['del']))
{
	if(array_key_exists("images",$_POST))
	{
		if(count($_POST['images'])>0)
		{
			for($i=0;$i<count($_POST['images']);$i++)
			{
				if(unlink($_POST['images'][$i]))
				{
					$status=1;
				}
			}
			if($status)
			{
				echo "<p class='alert alert-success'>$i Members deleted successfully</p>";
			}
		}
		else
		{
			echo "<p>Please select atleast one file to delete</p>";
		}
	}
	else
	{
		echo "<p class='alert alert-danger'>Please select atleast one file to delete</p>";
	}
}
?>
<form method="POST" action="">
<input type="submit" name="del" value="Delete">

<?php 
$folder="Click&Pledge";
if(file_exists($folder))
{
	$dp=opendir($folder);
	while($file = readdir($dp))
	{
		if(!($file=="." || $file==".."))
		{
			$ext=strpos($file,".");
						$playerName=substr($file,0,$ext);
			?>
			
			<div class="col-md-3">
			  <div class="thumbnail">
				  <img src="<?php echo $fpath."/".$file;?>" alt="<?php echo $playerName; ?>" style="width:200px; height:200px;">
				  <div class="caption">
					<p class="text-center"><input type="checkbox" name='images[]' value="<?php echo $fpath."/".$file;?>"><?php echo ucfirst($playerName); ?></p>
				  </div>
			  </div>
			</div>
			
			<!--<div>
				<img src="<?php echo $folder."/".$file?>" height="200" width="200">
				<p><?php echo $file;?></p>
			</div>-->
			<?php
		}
	}
}
else
{
	echo "Sorry";
}

?>
<input type="submit" name="del" value="Delete">
</form>
</div>
	</div>
</div>
</html>
<?php 
}
else
{
	header("Location:login.php");
}
?>
