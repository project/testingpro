<?php
ob_start();
session_start();
if(isset($_REQUEST['key']))
{
	$team = $_REQUEST['key'];
	if($team=="Click")
	{
		$team="Click&Pledge";
	}
	$folders=array(
		"9885776740"=>"Click&Pledge",
		"9000237738"=>"Accenture11",
		"8885444401"=>"Swayam",
		"9032760776"=>"SportsIt",
		"9581112311"=>"Ferventz",
		"9908244228"=>"PTG11"
	);

	function getFileCount($fpath)
	{
		if(file_exists($fpath))
		{
			$dp=opendir($fpath);
			$count=0;
			while($file=readdir($dp))
			{
				if(!($file=="." || $file==".."))
				{
					$count++;
				}
			}
		}
		else
		{
			$count=0;
		}
		return $count;
	}
	
	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Teams | Cricket Tournament</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="lib/jquery.mousewheel.pack.js?v=3.1.3"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="source/jquery.fancybox.pack.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.5" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		/*body {
			max-width: 700px;
			margin: 0 auto;
		}*/
	</style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.html">Cricket Tournament</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.html">Home</a></li>
      <li><a href="schedule.html">Schedule</a></li>
      <li><a href="teams.php">Teams</a></li>
	  <?php 
		if(isset($_SESSION['status']))
		{
			?>
				<li><a href="">Welcome Captain!</a></li>
				<li><a href="logout.php">Logout</a></li>
			<?php
		}
		else
		{
			?>
				<li><a href="login.php">Login</a></li>
			<?php
		}
	  ?>
      
    </ul>
  </div>
</nav>
  
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<h3><?php echo $team;?> Team Members</h3>
			<?php 
			if(getFileCount($team)<=0)
			{
				echo "<p class='alert alert-info'>Team not uploaded</p>";
			}
			?>
			<br>
			<?php 
			
			if(file_exists($team))
			{
				$dp=opendir($team);
				while($file = readdir($dp))
				{
					if(!($file=="." || $file==".."))
					{
						$ext=strpos($file,".");
						$playerName=substr($file,0,$ext);
					?>
					<div class="col-md-3">
					  <div class="thumbnail">
						  
						  <a class="fancybox" rel="gallery<?php echo $file;?>" href="<?php echo $team."/".$file;?>" title="<?php echo $playerName; ?>">
	<img src="<?php echo $team."/".$file;?>" alt="<?php echo $playerName; ?>" style="height:150px; width:150px;" />
						  
						  <div class="caption">
							<p class="text-center"><?php echo ucfirst($playerName); ?></p>
						  </div>
					  </div>
					</div>
					<?php 
					}
				}
			}
			?>
		</div>
		<div class="col-md-3">
		<br><br>
			<?php 
			foreach($folders as $mob=>$team)
			{
			?>
			
			  <div class="thumbnail" >
				<a href="members.php?key=<?php  echo $team; ?>">
				  <!--<img src="/w3images/lights.jpg" alt="<?php echo $team; ?>" style="width:100%">-->
				  <div class="caption">
					<p><?php echo $team; ?></p>
				  </div>
				</a>
			  </div>
			
			<?php 
			}
			?>
		</div>
	</div>		
</div>
</html>
<?php 
}
else
{
	exit("Sorry! Unable to display content");
}
ob_end_flush();
?>
